## Module for loading repositories group from gitlab server
## @package    git.load
## @author     Ruslan V. Maltsev (mrv.work.box@yandex.ru)
## @version    0.1
## @date       2020-
## @copyright  GNU Public License
##
# ----------------------------------------------------------------------------------------------------------
import os
import sys
import json
import socket
import subprocess
#
import cmdArgs
# ----------------------------------------------------------------------------------------------------------

## @brief Repository loader class 
##
## using shell git commands: git clone, git pull
class loader :
   def __init__ ( self, server_p, groupPath_p, configFilePath_p, groupId_p ) :
      self.__error = None
      # server up checking
      #socket_l = socket.socket()
      #conn.connect( self.__server, 80 )
      # file existing
      if not os.path.exists( configFilePath_p ) :
         self.__error = [ "[ERROR] Configuration file does not exists." ]
         return 
      configFileObj_l = open( configFilePath_p, mode='r', encoding="utf8" )
      if not configFileObj_l :
         self.__error = [ "[ERROR] Configuration file can not been opened." ]
         return 
      self.__groupsDict = json.load( configFileObj_l )
      configFileObj_l.close()
      # 
      self.__server = "git@" + server_p + ":" + groupPath_p
      self.__groupId = groupId_p
      
   def errorTxt ( self ) :
      return self.__error

   def isValid ( self ) :   
      return not self.__error
   
   def load ( self ) :
      if not self.isValid() :
         return
      cloneCommandWithoutProjectName_l = "git clone " + self.__server
      #fetchCommand_l = "git fetch origin"
      pullCommand_l = "git pull origin"
      repositoryFieldName_l = "reponame"
      finalDirnameFieldName_l = "surname"
      for groupId in [ self.__groupId ] if self.__groupId else self.__groupsDict.keys() :
         currentGourp_l = self.__groupsDict.get( groupId )
         if currentGourp_l :
            for currentProjectInfo in currentGourp_l :
               finalDirname_l = currentProjectInfo.get( finalDirnameFieldName_l )
               if os.path.exists( finalDirname_l ) :
                  print( "* updating \"" + finalDirname_l + "\" progect ..." )
                  os.chdir( finalDirname_l ) 
                  subprocess.call( pullCommand_l, stdout=sys.stdout, stderr=sys.stderr )
                  os.chdir( ".." ) 
               else :
                  repositoryName_l = currentProjectInfo.get( repositoryFieldName_l )
                  print( "* \"" + repositoryName_l + "\" progect cloning ..." )
                  subprocess.call( cloneCommandWithoutProjectName_l + "/" + repositoryName_l, 
                     stdout=sys.stdout, stderr=sys.stderr )
                  if os.path.exists( repositoryName_l ) :
                     os.rename( repositoryName_l, finalDirname_l )
# /class loader :

if "__main__" == __name__ :
   cmdArgumentsDescriptions_l = []
   cmdArgumentsDescriptions_l.append( cmdArgs.description( [ "-h", "--help" ], "help information" ) )
   cmdArgumentsDescriptions_l.append( cmdArgs.description( [ "-s", "--server-address" ], "git-server ip or url", "gitwork.ru" ) )
   cmdArgumentsDescriptions_l.append( cmdArgs.description( [ "-g", "--group-path" ], "group path on git-server", "" ) )
   cmdArgumentsDescriptions_l.append( cmdArgs.description( [ "-c", "--config-path" ], "path for configuration file with projects list for loading", "list.json" ) )
   cmdArgumentsDescriptions_l.append( cmdArgs.description( [ "-n", "--group-id" ], "loading projects group id from configuration file (all groups on default)", "" ) )
   cmdArguments_l = cmdArgs.arguments( cmdArgumentsDescriptions_l )
   
   if cmdArguments_l.value( "--help" ):
      print( "\nUtility for loading projects set from git-server\n" )
      print( cmdArguments_l.helpTxt() )
      exit( 0 )

   loader = loader( cmdArguments_l.value( "--server-address" ), cmdArguments_l.value( "--group-path" ), 
      cmdArguments_l.value( "--config-path" ), cmdArguments_l.value( "--group-id" ) )

   loader.load()
