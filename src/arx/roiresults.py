# ----------------------------------------------------------------------------------------------------------
__author__ = 'Ruslan V. Maltsev'
""" Интерфейс запуска 
   roiresults [<путь к проверяемому каталогу 1>] [<путь к проверяемому каталогу 2>] ...
   
   Указывается каталоги для которого выполнять проверку наличия подготовленных материалов по лабораторным 
   работам дисциплины "Распределённая обработка информации". Вызов программы без параметров выполняет 
   проверку для текущего каталога.
"""
# ----------------------------------------------------------------------------------------------------------
import roimake

import os
import sys
# ----------------------------------------------------------------------------------------------------------

checkingFoldersList = sys.argv[ 1: ]
if not checkingFoldersList:
   checkingFoldersList.append( os.curdir )

# структура отчётных материалов
materialsNames = [ "отчёт", "титульный лист", "исходный текст программы" ]
labWorksMaterials = dict()
for labNumber in range( 1, 13 ):
   materialsList = list()
   materialsList.append( roimake.vsProjectOperator.formLabFolderName( labNumber ) + "\\report.docx" )
   materialsList.append( "" ) # титульный лист
   nextProjectName = roimake.vsProjectOperator.number2name( labNumber )
   if nextProjectName:
      materialsList.append( roimake.vsProjectOperator.formProjectFilePath( labNumber, 
         roimake.vsProjectOperator.addExtention2ProjectFileName( nextProjectName ) ) )
   labWorksMaterials[ labNumber ] = materialsList
# ----------------------------------------------------------------------------------------------------------

delimeter = ";"
for nextFolder in checkingFoldersList :
   if not os.path.exists( nextFolder ) :
      sys.stdout.write( '\n[WARNING] Path "' + nextFolder + '" does not exists.' )
      continue
   if not os.path.isdir( nextFolder ):
      sys.stdout.write( "\n[WARNING] Object \"" + nextFolder + "\" is not a directory." )
      continue
   sys.stdout.write( "\n" + delimeter + nextFolder )
   hiHeadersTxt = "\n" + delimeter * 2
   for number, materials in labWorksMaterials.items() :
         hiHeadersTxt += "л/р №" + str( number ) + ( delimeter * len( materials ) )
   sys.stdout.write( hiHeadersTxt )
   loHeadersTxt = "\n" + delimeter + "Ф.И.О." + delimeter
   for number, materials in labWorksMaterials.items() :
      for i in range( 0, len( materials ) ) :
         loHeadersTxt += materialsNames[ i ] + delimeter
   sys.stdout.write( loHeadersTxt )
   for nextSubObject in os.listdir( nextFolder ) :
      if os.path.isdir( nextFolder + "\\" + nextSubObject ) :
         nextResultTxt = "\n" + delimeter + nextSubObject + delimeter
         for number, materials in labWorksMaterials.items() :
            for materialPath in materials:
               nextResultTxt += ( "+" if materialPath and os.path.exists( nextFolder + "\\" + nextSubObject 
                  + "\\" + materialPath ) else "-" ) + delimeter
         sys.stdout.write( nextResultTxt )
   sys.stdout.write( "\n" )