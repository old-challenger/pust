## Configured MS VS solution making 
## @package    vs.make 
## @author     Ruslan V. Maltsev (mrv.work.box@yandex.ru)
## @version    0.2
## @date       2020-
## @copyright  GNU Public License
##
## Интерфейс запуска 
## @n python vs\make.py -s <structure> -d <directory> <projects or solutions ids list>
## @n В качестве идентификаторов eказываются номера лабораторных работ, названия решений (solution), проектов, 
## для которых необходимо выполнить сборку. Вызов программы без параметров выполняет сборку всех проектов 
## для заданной структуры.
## @n Данный файл также может быть использован в качестве библиотечного модуля для организации сборки проекта. 
## Примером использования библиотечных класоов является код приведённый после условия if "__main__" == __name__ :
##
# ----------------------------------------------------------------------------------------------------------
import os
import re
import sys
import json
import winreg
import subprocess
import xml.etree.ElementTree
#
import cmdArgs
# ----------------------------------------------------------------------------------------------------------

class xmlCheckingElementValues :
   
   class regexDescriptor :

      def __init__ ( self, id_p ) :
         self.__reDescriptor = id_p

      def __eq__ ( self, value_p ) :
         return bool( self.__reDescriptor.match( value_p ) )

   def __init__ ( self, name_p, label_p = None, value_p = None, namespace_p = "", 
                        autocorrection_p = False, required_p = False ) :
      self.__autocorrection = autocorrection_p
      self.__childs = list()
      self.__label = label_p
      self.__namespace = namespace_p
      self.__name = name_p
      self.__required = required_p
      self.__value = value_p if not value_p or isinstance( value_p, str ) \
                        else xmlCheckingElementValues.regexDescriptor( value_p )
   
   def addChild ( self, element_p ) :
      self.__childs.append( element_p )
      
   def autocorrection ( self ) :
      return self.__autocorrection
      
   def childs ( self ) :
      return self.__childs
   
   def formNamespace ( self, fullName_p ) :
      nameLen_l = len( self.__name )
      assert( self.__name == fullName_p[ -nameLen_l : ] )
      self.__namespace = fullName_p[ : -nameLen_l ]
      return self.__namespace
   
   def name ( self ) :
      return self.__namespace + self.__name
      
   def namespace ( self ) :
      return self.__namespace
   
   def label ( self ) :
      return self.__label
   
   def rawName ( self ) :
      return self.__name
      
   def required ( self ) :
      return self.__required
   
   def value ( self ) :
      return self.__value
   
   def __eq__ ( self, element_p ) :
      value_l = element_p.get( self.__label ) if self.__label else element_p.text
      return value_l and value_l == self.__value

class vsProjectOperator :
   
   def build( self, vsBuildUtilityPath_p ) :
      sys.stdout.write( '\n\t[ ** ] "' + self.__projectName + '" project building...' )
      solutionFilePath_l = self.__formSolutionFilePath()
      if not os.path.exists( solutionFilePath_l ) :
         sys.stdout.write( "\n\t\t[ ERROR ] Required solution file does not exists: " )
         sys.stdout.write( '\n\t\t\t "' + os.path.abspath( solutionFilePath_l ) + '".' )
         return
      self.__projectFilePath = self.__formProjectFilePath()
      if not os.path.exists( self.__projectFilePath ) :
         self.__projectFilePath = self.__formProjectFilePath()
         if not os.path.exists( self.__projectFilePath ) :
            sys.stdout.write( "\n\t\t[ ERROR ] Required project file does not exists: " )
            sys.stdout.write( '\n\t\t\t "' + os.path.abspath( self.__projectFilePath ) + '".' )
            return
      runParameters_l = [ vsBuildUtilityPath_p, solutionFilePath_l, "/rebuild", \
            self.__buildConfiguration, "/project", self.__projectName ]
      runingString_l = str()
      for next in runParameters_l :
         runingString_l += next + " "
      sys.stdout.write( '\n\t\t' + runingString_l )
      sys.stdout.write( "\n\t\t[ *** ] MS VS output begin: \n" )
      sys.stdout.flush()
      if not self.__checkProjectFile() :
         subprocess.call( runParameters_l )
      sys.stdout.write( "\n\t\t[ *** ] MS VS output end.\n" )
      sys.stdout.flush()
   
   def cleanLabProjectDirrectory ( self ) :
      subprocess.call( [ "cleansrc.cmd", self.__solutionPath ] )
      sys.stdout.write( "\n\t\"" + os.path.abspath( self.__solutionPath ) + "\" directory was cleaned;\n" )
      sys.stdout.flush()
   
   def isValid ( self ) :
      return self.__valid
      
   def projectName ( self ) :
      return self.__projectName
      
   def projectsPath ( self ) :
      return self.__projectsPath
      
   def solutionName ( self ) :
      return self.__solutionName
      
   def solutionPath ( self ) :
      return self.__solutionPath
      
   #def labNumber ( self ) :
   #   return self.__labNumber
      
   def __init__ ( self, project_p, solutionDescription_p ) :
      self.__valid = False
      projectsList_l = solutionDescription_p.get( "projects" )
      if not projectsList_l or not isinstance( projectsList_l, list ) :
         return 
      self.__projectName = projectsList_l[ project_p ] if isinstance( project_p, int ) else project_p
      self.__solutionName           = solutionDescription_p.get( "solution" )
      self.__solutionPath           = solutionDescription_p.get( "solutionPath" )
      self.__projectsPath           = solutionDescription_p.get( "projectsPath" )
      self.__buildConfiguration     = solutionDescription_p.get( "configuration" )
      self.__platformVersionValue   = solutionDescription_p.get( "platformVersionValue" )
      self.__intDirValue            = solutionDescription_p.get( "intDirValue" )
      self.__debugOutDirValue       = solutionDescription_p.get( "debugOutDirValue" )   
      self.__releaseOutDirValue     = solutionDescription_p.get( "releaseOutDirValue" )  
      self.__hierarchy              = len( projectsList_l ) > 1
      self.__valid = self.__solutionName and self.__solutionPath and self.__projectsPath        \
         and self.__buildConfiguration and self.__platformVersionValue and self.__intDirValue   \
         and self.__debugOutDirValue and self.__releaseOutDirValue
      
   def __checkProjectFile( self ) :
      msNamespace_l = "http://schemas.microsoft.com/developer/msbuild/2003"
      root_l = xmlCheckingElementValues( "Project" )
      xml.etree.ElementTree.register_namespace( '', msNamespace_l )
      tree_l = xml.etree.ElementTree.parse( self.__projectFilePath )
      rootElement_l = tree_l.getroot()
      root_l.formNamespace( rootElement_l.tag )
      assert( "{%s}" % msNamespace_l == root_l.namespace() )
      globals_l = xmlCheckingElementValues( "PropertyGroup", "Label", "Globals", root_l.namespace() )
      globals_l.addChild( xmlCheckingElementValues(   "WindowsTargetPlatformVersion", 
                                                      value_p = self.__platformVersionValue, 
                                                      namespace_p = root_l.namespace(), 
                                                      autocorrection_p = True, required_p = True ) )
      configurationRelease_l = xmlCheckingElementValues( "PropertyGroup", "Condition", 
         # tagname:  PropertyGroup; propertyname: Condition; 
         # propertyvalue: '$(Configuration)|$(Platform)'=='Release|'
         re.compile( "^'\$\(Configuration\)\|\$\(Platform\)'=='Release\|" ), root_l.namespace() )
      configurationDebug_l = xmlCheckingElementValues( "PropertyGroup", "Condition", 
         # tagname:  PropertyGroup; propertyname: Condition; 
         # propertyvalue: '$(Configuration)|$(Platform)'=='Debug|'
         re.compile( "^'\$\(Configuration\)\|\$\(Platform\)'=='Debug\|" ), root_l.namespace() )
      configurationRelease_l.addChild( xmlCheckingElementValues(  
         "OutDir", value_p = self.__releaseOutDirValue, namespace_p = root_l.namespace() ) )
      configurationDebug_l.addChild( xmlCheckingElementValues( 
         "OutDir", value_p = self.__debugOutDirValue, namespace_p = root_l.namespace() ) )
      intDir_l = xmlCheckingElementValues( "IntDir", value_p = self.__intDirValue, namespace_p = root_l.namespace() )
      configurationRelease_l.addChild( intDir_l )
      configurationDebug_l.addChild( intDir_l )
      #
      result_l = 0
      writeCorrections2File_l = False
      checkingsRoot_l = ( globals_l, configurationRelease_l, configurationDebug_l ) 
      for rootElement in rootElement_l.findall( globals_l.name() ) :
         for rootChecker in checkingsRoot_l :
            if rootElement == rootChecker :
               for checker in rootChecker.childs() :
                  currentElement_l = rootElement.find( checker.name() )
                  if None == currentElement_l :
                     if checker.required() :
                        sys.stdout.write( "\n[ERROR] no tag <" + checker.name() + "> in \"" 
                           + self.__projectFilePath + "\" project file." )
                        result_l |= 0x1
                     continue
                  if currentElement_l != checker :
                     if checker.autocorrection() :
                        currentElement_l.text = checker.value()
                        sys.stdout.write( "\n[WARNING] Tag <" + checker.rawName() + "> text will be corrected in project file." )
                        writeCorrections2File_l = True
                     else :
                        sys.stdout.write( "\n[ERROR] Incorrect tag <" + checker.rawName() + "> value in \""
                           + self.__projectFilePath + "\" project file.\n\tMust be this: \"" + checker.value() + "\"" )
                        result_l |= 0x2
                  sys.stdout.flush() 
      if writeCorrections2File_l :
         tree_l.write( self.__projectFilePath, xml_declaration = True, encoding = "utf-8" )
      return result_l

   def __formProjectFilePath ( self ) :
      return self.__projectsPath \
         + ( ( self.__projectName + "\\" + self.__projectName ) if self.__hierarchy else self.__projectName ) \
         + ".vcxproj"

   def __formSolutionFilePath ( self ) :
      return self.__solutionPath + self.__solutionName + ".sln"

class labDescriptions :

   @staticmethod
   def incorrectArgumetPrint ( argumentTxtValue_p ) :
      sys.stdout.write( '\n[ WARNIG ] Icorrect argument value \"' + argumentTxtValue_p + '\"' )
   
   @staticmethod
   def loadDataFromJson ( filePath_p, errorTxt_p ) :
      if not os.path.exists( filePath_p ) :
         return "[ERROR] " + errorTxt_p + " file does not exists." 
      fileObj_l = open( filePath_p, mode='r', encoding="utf8" )
      if not fileObj_l :
         return "[ERROR] " + errorTxt_p + " file can not been opened." 
      result_l = json.load( fileObj_l )
      fileObj_l.close()
      return result_l

   def formProjectsForMaking ( self, cmdArguments_p = [] ) :
      projectsDescriptors_l = cmdArguments_p if cmdArguments_p else self.__projectsNumbers2NamesMap.keys()
      result_l = list()
      for i in projectsDescriptors_l :
         result_l += self.__createProjectsOperatorsListByDescrition( str( i ) )
      return result_l 

   def name2number ( self, name_p ) :
      if not self.__projectsNames2NumbersMap :
         self.__projectsNames2NumbersMap = dict()
         for ( labNumber, names ) in self.__projectsNumbers2NamesMap.items() :
            if isinstance( names, tuple ):
               for projectName in names :
                  self.__projectsNames2NumbersMap[ projectName ] = labNumber
            else :
               self.__projectsNames2NumbersMap[ names ] = labNumber
      return self.__projectsNames2NumbersMap.get( name_p )
      
   def number2name ( self, number_p ) :
      return self.__projectsNumbers2NamesMap.get( number_p )

   def __init__ ( self, configMap_p ) :
      self.__projectsNumbers2NamesMap = configMap_p
      self.__formNames2NumbersMap()

   def __createProjectsOperatorsList ( self, projectsNamesList_p, solutionDescription_p ) :
      result_l = list()
      for projectName in projectsNamesList_p :
         newProjectOperator_l = vsProjectOperator( projectName, solutionDescription_p )
         if newProjectOperator_l.isValid() :
            result_l.append( newProjectOperator_l )
         else :
            sys.stdout.write( '\n[ WARNIG ] Icorrect descrition for project \"' + projectName + '\".' )
      return result_l
      
   def __createProjectsOperatorsListByDescrition ( self, descriptionTxt_p ) :
      if descriptionTxt_p.isdigit() :
         solutionNumber_l = descriptionTxt_p
         solutionDescription_l = self.__projectsNumbers2NamesMap.get( solutionNumber_l )
         if None == solutionDescription_l :
            self.incorrectArgumetPrint( descriptionTxt_p )
            return list()
         projectsList_l = solutionDescription_l[ "projects" ]
      else :
         solutionNumber_l = self.__projectsNames2NumbersMap.get( descriptionTxt_p )
         if None == solutionNumber_l :
            self.incorrectArgumetPrint( descriptionTxt_p )
            return list()
         solutionDescription_l = self.__projectsNumbers2NamesMap.get( solutionNumber_l )
         if descriptionTxt_p == solutionDescription_l[ "solution" ] :
            projectsList_l = solutionDescription_l[ "projects" ]
         elif descriptionTxt_p in solutionDescription_l[ "projects" ] :
            projectsList_l = [ descriptionTxt_p ]
         else :
            self.incorrectArgumetPrint( descriptionTxt_p )
            return list()
      return self.__createProjectsOperatorsList( projectsList_l, solutionDescription_l )

   def __formNames2NumbersMap ( self ) :
      self.__projectsNames2NumbersMap = dict()
      for ( labNumber, parameters ) in self.__projectsNumbers2NamesMap.items() :
         if not parameters[ "solution" ] in self.__projectsNames2NumbersMap :
            self.__projectsNames2NumbersMap[ parameters[ "solution" ] ] = labNumber
         for  projectName in parameters[ "projects" ] :
            if not projectName in self.__projectsNames2NumbersMap :
               self.__projectsNames2NumbersMap[ projectName ] = labNumber
      
class vsPaths :
   class errorsValues :
      noErrors                         = 0x0
      #installDirrectoryNotFound        = 0x1
      installDirrectoryDoesNotExists   = 0x2
      buildUtilityDoesNotExists        = 0x4
   
   def __init__ ( self ) :
      self.__errors = self.errorsValues.noErrors
      self.__getVSpath()
      self.__getDevEnvPath()

   def errors( self ) :
      return self.__errors

   def isValid ( self ) :
      return self.errorsValues.noErrors == self.__errors
      
   def installDirrectoryPath ( self ) :
      return self.__installDirrectory
      
   def buildUtilityPath ( self ) :
      return self.__buildUtilityPath
      
   def __getVSpath ( self ):
      rootKey_l = winreg.HKEY_LOCAL_MACHINE #winreg.HKEY_CURRENT_USER
      regKeyPath_l = "SOFTWARE\\WOW6432Node\\Microsoft\\VisualStudio\\SxS\\VS7" 
      regKeyName_l = "15.0" #"ShellFolder"
      sys.stdout.write( '\n[ * ] Searching Microsoft Visual Studio ...' )
      registryKey_l = winreg.OpenKey( rootKey_l, regKeyPath_l )
      self.__installDirrectory = winreg.QueryValueEx( registryKey_l, regKeyName_l )[ 0 ]
      sys.stdout.write( '\n\tInstall directory for Microsoft Visual Studio is: ' \
         + '\n\t\t"' + self.__installDirrectory + '"' )
      if not os.path.exists( self.__installDirrectory ):
         sys.stdout.write( "\n[ ERROR ] Microsoft Visual Studio install directory does not exists." )
         self.__errors |= self.errorsValues.installDirrectoryDoesNotExists

   def __getDevEnvPath ( self ):
      sys.stdout.write( '\n[ * ] Checking Microsoft Visual Studio development environmet existing...' )
      self.__buildUtilityPath = self.__installDirrectory + "Common7\\IDE\\devenv.com"
      if not os.path.exists( self.__buildUtilityPath ):
         sys.stdout.write( "\n[ ERROR ] Required executable file does not exists: " )
         sys.stdout.write( '\n\t "' + self.__buildUtilityPath + '".' )
         self.__errors |= self.errorsValues.buildUtilityDoesNotExists
      
if "__main__" == __name__ :
   minusesTxt_l = "-" * 13
   sys.stdout.write( "\n " + minusesTxt_l + " VS projects making program " + minusesTxt_l )
   # cmd arguments processing
   cmdArgumentsDescriptions_l = []
   cmdArgumentsDescriptions_l.append( cmdArgs.description( [ "-h", "--help" ], "help information typing" ) )
   cmdArgumentsDescriptions_l.append( cmdArgs.description( [ "-s", "--solution-structure" ], 
      "path for configuration file with solution structure description", "" ) )
   cmdArgumentsDescriptions_l.append( cmdArgs.description( [ "-d", "--target-directory" ], 
      "target directory for compilation", "." ) )
   cmdArguments_l = cmdArgs.arguments( cmdArgumentsDescriptions_l )
   if cmdArguments_l.value( "--help" ) :
      sys.stdout.write( """\n makes compilation of solution in targeted directory with prescribed structure
         \t\t python vs\make.py -s <structure> -d <directory> <projects or solutions ids list>
      """ )
      sys.stdout.write( cmdArguments_l.helpTxt() )
      exit( 0 )
   # MS VS installation checking
   vsPaths_l = vsPaths()
   if not vsPaths_l.isValid() :
      exit( - vsPaths_l.errors() )  
   # target directory checking
   targetDirectory_l = cmdArguments_l.value( "--target-directory" )
   if not os.path.exists( targetDirectory_l ) :
      sys.stdout.write( '\n[ ERROR ] Directory "' + os.path.abspath( targetDirectory_l ) + '" does not exists.' )
      exit( -8 )
   startDirectory_l = os.path.abspath( '.' )
   # projects and solutions ids processing
   solutionStructure_l = labDescriptions.loadDataFromJson( cmdArguments_l.value( "--solution-structure" ), "project structure" )
   if isinstance( solutionStructure_l, str ) :
      sys.stdout.write( solutionStructure_l )
      exit( -16 )
   projectsForMaking = labDescriptions( solutionStructure_l ).formProjectsForMaking( cmdArguments_l.unknownList() )
   if not projectsForMaking:
      sys.stdout.write( '\n[ ERROR ] Making projects list is empty' )
      exit( -1 )
      sys.stdout.flush()      
   # compilation
   os.chdir( targetDirectory_l )
   sys.stdout.write( "\n[ * ] Following projects will be compiled:" )
   for next in projectsForMaking:
      sys.stdout.write( '\n\t project "' + next.projectName() + '"\tin solution "' + next.solutionName() )\
         #+ '"\tfrom labwork №' + str( next.labNumber() ) )      
   sys.stdout.write( "\n[ * ] Cleaning directories ..." )
   sys.stdout.flush()
   for next in projectsForMaking:
      next.cleanLabProjectDirrectory()
   sys.stdout.write( "\n[ * ] Projects building..." )
   for next in projectsForMaking:
      next.build( vsPaths_l.buildUtilityPath() )
   os.chdir( startDirectory_l )