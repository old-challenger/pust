# ----------------------------------------------------------------------------------------------------------
__author__ = 'Ruslan V. Maltsev'
""" Интерфейс запуска 
   roirun 1 2 3 5 
   
   Указываются номера лабораторных работ или названия решений (solution), для которых необходимо выполнить 
   сборку. Вызов программы без параметров выполняет сборку всех проектов.
   
   Текущая версия выполняет сборку следующих проектов:
      generator      - лабораторная работа №1 ( параметр 1 или generator )
      monothread     - лабораторная работа №2 ( параметр 2 или monothread )
      multithread    - лабораторная работа №3 ( параметр 3 или multithread )
      mapreduce      - лабораторная работа №5 ( параметр 5 или mapreduce )
"""
# ----------------------------------------------------------------------------------------------------------
import cmdArgs
import roimake

import os
import sys
import subprocess
# ----------------------------------------------------------------------------------------------------------

def run ( projectName_p, params_p ):
   sys.stdout.write( '\n ------- "' + projectName_p + '" program running... ----------- ' )   
   runningFile_l = projectName_p + ".exe"
   if not os.path.exists( runningFile_l ):
      sys.stdout.write( '\n[ ERROR ] Required executable file "' + runningFile_l + '" does not exists.' )
      return
   allParams_l = [ runningFile_l ] + params_p
   runningString_l = str()
   for next in allParams_l:
      runningString_l += next + ' '
   sys.stdout.write( '\n Running command: \n   ' + runningString_l )
   subprocess.call( allParams_l )
   sys.stdout.write( '\n ------- "' + projectName_p + '" program finished. ----------- \n' )

def smartRun ( projectName_p, imperativeParamsList_p, aditionalParamsList_p ):
   assert( isinstance( imperativeParamsList_p, list ) )
   assert( isinstance( aditionalParamsList_p, list ) )
   imperativeParamsSet_l = imperativeParamsList_p
   run( projectName_p, imperativeParamsList_p 
      + [ next for next in aditionalParamsList_p if not next in imperativeParamsSet_l ] )
   
sys.stdout.write( "\n ------------- ROI labworks projects running programm ------------- " )
cmdArgumentsDescriptions_g = []
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-h", "--help" ], "help information" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-a", "--additional-params" ], 
   "additional parameters for runing programs", "" ) )
cmdArguments_g = cmdArgs.arguments( cmdArgumentsDescriptions_g )
if cmdArguments_g.value( "--help" ):
   sys.stdout.write( "\n" + cmdArguments_g.helpTxt() )
   exit( 0 )

projectsForMaking = roimake.vsProjectOperator.formProjectsForMaking( cmdArguments_g.unknownList() )
if not projectsForMaking:
   sys.stdout.write( '\n[ ERROR ] Making projects list is empty' )
   exit( -1 )
sys.stdout.write( "\n[ * ] Following projects will be run:" )
for next in projectsForMaking:
   sys.stdout.write( '\n\t"' + next.projectName() + '"\tin solution "' + next.solutionName() \
      + '"\tfrom labwork №' + str( next.labNumber() ) )

sys.stdout.write( "\n[ * ] Running..." )
additionalParams = cmdArguments_g.value( "--additional-params" )
imperativeParamsMap = {
   "generator"    : [ "--config", "parameters.cfg", "--output", "test-data" ],
   "monothread"   : [ "--input", "test-data", "--output", "monothread-result.csv" ],
   "multithread"  : [ "--input", "test-data", "--output", "multithread-result.csv" ],
   "mapreduce"    : [ "--input", "test-data", "--output", "mapreduce-result.csv" ]
}
for next in projectsForMaking:
   smartRun( next.projectName(), imperativeParamsMap[ next.projectName() ], additionalParams.split() )
