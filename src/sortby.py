## Utility for sort files lines with delimeted fields by one or more fields.
## @package    sortby 
## @author     Ruslan V. Maltsev (mrv.work.box@yandex.ru)
## @version    0.2
## @date       2015-
## @copyright  GNU Public License
##
# ----------------------------------------------------------------------------------------------------------
import os
import sys
import cmdArgs
# ----------------------------------------------------------------------------------------------------------

## @brief (key, data)-pair consists input line data
class linePairForSort:
   
   def __init__ ( self, key_p, data_p ):
      self.__key = key_p
      self.__data = data_p

   def key ( self ):
      return self.__key

   def data ( self ):
      return self.__data

   def __cmp__ ( self, other_p ):
      if self.__key < other_p.key():
         return -1
      elif self.__key > other_p.key():
         return 1
      else:
         return 0
# /class linePairForSort:

## @brief key format class for sorting input lines
class keyFormat_class:
   
   def __init__ ( self, delimeter_p, outDelimeter_p, literalHeaderSize_p, sortingFiledsString_p ):
      self.__fullLineId = -1
      self.__delimeter = delimeter_p                  # fields delimeter
      self.__outDelimeter = outDelimeter_p            # out fileds delimeter
      self.__literalHeaderSize = literalHeaderSize_p  # literal headers size
      self.__valid = True                             # validation of sorting fields parameter format
      self.__filedsNumbers = []                       # sorting fields parameter format after parsing process
      for field in sortingFiledsString_p.split( "," ):
         if field.isdigit():
            self.__filedsNumbers.append( int( field ) - 1 )
         else:
            digitPair_l = field.split("-")
            if 2 != len( digitPair_l ) or not digitPair_l[ 0 ].isdigit() or not digitPair_l[ 1 ].isdigit():
               self.__valid = False
               break
            fieldsInterval_l = int( digitPair_l[ 0 ] ), int( digitPair_l[ 1 ] )
            if fieldsInterval_l[ 0 ] > fieldsInterval_l[ 1 ] or ( 0, 0 ) == fieldsInterval_l:
               self.__valid = False
               break
            for fieldNumber in range( fieldsInterval_l[ 0 ] - 1, fieldsInterval_l[ 1 ] ):
               self.__filedsNumbers.append( fieldNumber )

   def isValid ( self ):
      return self.__valid
      
   def makeLinePair ( self, line_p ):
      result_l = str()
      fields_l = line_p.split( self.__delimeter )
      for fieldNumber in self.__filedsNumbers:
         if self.__fullLineId == fieldNumber:
            field_l = line_p
         elif fieldNumber < len( fields_l ):
            field_l = fields_l[ fieldNumber ]
         else:
            return None
         result_l += field_l
      if self.__delimeter == self.__outDelimeter:
         return result_l, line_p
      else:
         outputLine_l = fields_l[ 0 ]
         for field in fields_l[1:]:
            outputLine_l += self.__outDelimeter + field
         return result_l, outputLine_l

   def makeLiteralHeader ( self, key_p ):
      if self.__literalHeaderSize > 0:
         if self.__literalHeaderSize > len( key_p ):
            return key_p
         else:
            return key_p[:literalHeadersSize_g]
      else:
         return None

   def makeLiteralHeaderLine ( self, literalHeader_p ):
      result_l = str()
      for i in range( 81 ):
         result_l += "*"
      result_l += " " + literalHeader_p.upper() + "\n"
      return result_l
# /class keyFormat_class

# cmd arguments descriptons forming
cmdArgumentsDescriptions_g = []
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-h", "--help" ], "help information" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-d", "--fields-delimeter" ],
   "fields delimeter in the input file lines", ";" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-i", "--input-file" ],
   "path to the file with input data", "" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-f", "--sorting-fields" ],
   "fields for sorting: 2 or 1,3,5 or 2-4 or 1,3-6,10; 0 - it's all line", "1" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-o", "--output-file" ],
   "path to the file with input data", "" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-c", "--code-page" ],
   "path to the file with input data", "cp1251" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-l", "--literal-header" ],
   "Literal headers size (if 0 - no headers)", "" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "--out-code-page" ],
   "path to the file with input data", "--code-page" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "--out-field-delimeter" ],
   "fields delimeter in the output file lines", "--fields-delimeter" ) )
cmdArguments_g = cmdArgs.arguments( cmdArgumentsDescriptions_g )

# checking help-parameter defined
if cmdArguments_g.value( "--help" ):
   print( aboutText_g )
   print( cmdArguments_g.helpTxt() )
   exit( 0 )

# checking unknown paramets list
if len( cmdArguments_g.unknownList() ):
   print( "[WARNING] " + cmdArguments_g.unknown() )

#processing key fileds
filedsDelimeter_g = cmdArguments_g.value( "--fields-delimeter" )
outFieldsDelimeter_g = cmdArguments_g.value( "--out-field-delimeter" )
if "--fields-delimeter" == outFieldsDelimeter_g:
   outFieldsDelimeter_g = filedsDelimeter_g
sortingFiledsArg_g = cmdArguments_g.value( "--sorting-fields" )
literalHeadersSize_g = cmdArguments_g.value( "--literal-header" )
if not len( literalHeadersSize_g ):
   literalHeadersSize_g = 0
elif literalHeadersSize_g.isdigit():
   literalHeadersSize_g = int( literalHeadersSize_g )
else:
   print( "\n[ERROR] Incorrect --literal-header value " + str( literalHeadersSize_g ) + ".\n" )
   exit( 1 )
keyFormat_g = keyFormat_class( filedsDelimeter_g, outFieldsDelimeter_g, literalHeadersSize_g, sortingFiledsArg_g )
if not keyFormat_g.isValid():
   print( "\n[ERROR] Incorrect format of --sorting-fields parameter \"" + sortingFiledsArg_g + "\".\n" )
   exit( 2 )

# checking input source existing
inTextEncoding_g = cmdArguments_g.value( "--code-page" )
if "--code-page" == cmdArguments_g.value( "--out-code-page" ):
   outTextEncoding_g = inTextEncoding_g
else:
   outTextEncoding_g = cmdArguments_g.value( "--out-code-page" )

inputFileName_g = cmdArguments_g.value( "--input-file" )
if len( inputFileName_g ):
   if not os.path.exists( inputFileName_g ):
      print( "\n[ERROR] File \"" + inputFileName_g + "\" is not exists.\n"  )
      exit( 3 )
   input_g = open( inputFileName_g, "r", encoding = inTextEncoding_g )
else:
   if inTextEncoding_g != sys.stdin.encoding:
      print( "\n[WARNING] Incorrect output encoding in standart input stream.\n" )
   input_g = sys.stdin

# forming (key, data) pairs from input lines
currentLineNumber_g = 1
result_g = []
while ( True ):
   nextLine_g = input_g.readline()
   if ( not nextLine_g ):
      break
   nextPair_l = keyFormat_g.makeLinePair( nextLine_g )
   if ( None == nextPair_l ):
      print( "\n[ERROR] Incorrect filed number for line " + str( currentLineNumber_g ) + ".\n" )
      exit( 4 )
   result_g.append( nextPair_l )
   currentLineNumber_g += 1
if ( sys.stdin != input_g ):
   input_g.close()
result_g.sort()

# outputing result
outputFileName_g = cmdArguments_g.value( "--output-file" )
if len( outputFileName_g ):
   output_g = open( outputFileName_g, "w", encoding = outTextEncoding_g )
   if ( None == output_g ):
      print( "\n[ERROR] Can not open file \"" + outputFileName_g + "\" for writing.\n" )
      exit( 5 )
else:
   if outTextEncoding_g != sys.stdout.encoding:
      print( "\n[WARNING] Incorrect output encoding in standart output stream.\n" )
   output_g = sys.stdout

literalHeader_g = None
for line in result_g:
   if literalHeadersSize_g:
      lineLiteralHeader_l = keyFormat_g.makeLiteralHeader( line[ 0 ] )
      if literalHeader_g != lineLiteralHeader_l:
         literalHeader_g = lineLiteralHeader_l
         output_g.write( keyFormat_g.makeLiteralHeaderLine( literalHeader_g ) )
   output_g.write( line[ 1 ] )
if ( sys.stdout != output_g ):
   output_g.close()