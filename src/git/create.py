## Module for creating repositories group on gitlab server
## @package    git.create 
## @author     Ruslan V. Maltsev (mrv.work.box@yandex.ru)
## @version    0.01
## @date       2020-
## @copyright  GNU Public License
##
# ----------------------------------------------------------------------------------------------------------
import sys
import json
import subprocess
# ----------------------------------------------------------------------------------------------------------
import gitlab
# ----------------------------------------------------------------------------------------------------------
tokenFilePath = "w:\\db\\security\\tokens\\gitwork.ru.json"
usersFilePath = "w:\\work\\teaching\\2019-2020\\пасоиб\\5k.json"
groupName = "results/vkr" # "results/roi"

# reading token data file
tokenFileObj = open( tokenFilePath, mode='r', encoding="utf8" )
tokenData = json.load( tokenFileObj )
tokenFileObj.close()

tokenValue = tokenData.get( "value" )
serverProtocol = tokenData.get( "protocol" )
serverUrl = tokenData.get( "server-url" )
if not ( tokenValue and serverProtocol and serverUrl ) :
   print( "[ERROR] incorrect token file format" )
   exit( -1 )

# reading users data file
usersFileObj = open( usersFilePath, mode='r', encoding="utf8" )
usersData = json.load( usersFileObj )
usersFileObj.close()

# connection and groups getting
serverConnection = gitlab.Gitlab( serverProtocol + '://' + serverUrl, private_token = tokenValue )
try :
   currentGroup = serverConnection.groups.get( groupName )
except Exception :
   print( '[ERROR] no group "' + groupName + '" on server "' + serverUrl + '"' )
   exit( -2 )
  
for groupId, groupsUsersList in usersData.items() :
   for nextUserData in groupsUsersList :
      newProjectName = nextUserData.get( "reponame" )
      currentUserName = nextUserData.get( "fullname" )
      currentUserNickname = nextUserData.get( "nickname" )
      if not ( newProjectName and currentUserName and currentUserNickname ) :
         print( '[WARNING] incorrect user data:\n' + str( nextUserData ) + '\n[WARNING] project was not created.' )
         continue
      queryResult = serverConnection.users.list( search = currentUserName )
      # вставить проверки
      if 1 != len( queryResult ) :
         print( '[WARNING] incorrect search result on username "' + currentUserName 
            + '". project was not created.' )
         continue
      currentUser = queryResult[ 0 ]
      if currentUser.username != currentUserNickname :
         print( "[WARNING] configuration file disparity on nickname: " + currentUser.username + "!=" + currentUserNickname )
      print( '* creating repository "' + newProjectName + '" for user "' + currentUser.name + '".' )
      queryResult = serverConnection.projects.list( search = newProjectName )
      if queryResult :
         doNotCreateFlag = False
         for existingProject in queryResult :
            fullPath = existingProject.namespace.get( "full_path" )
            if fullPath and fullPath == groupName :
               doNotCreateFlag = True
               break
         if doNotCreateFlag :
            print( '[WARNING] project "' + newProjectName + '" allready exists and consequently was not created.' )
            continue
      newProject = serverConnection.projects.create( {  
         'name'         : newProjectName,
         'description'  : groupName + ' final results repository', # ' course final results repository',
         'namespace_id' : currentGroup.id } )
      newProject.members.create( { 'user_id' : currentUser.id, 'access_level' : gitlab.MAINTAINER_ACCESS } )
      print( '[OK] project "' + newProjectName + '" was created.' )