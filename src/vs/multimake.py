## Configured MS VS solution groups making 
## @package    vs.multimake 
## @author     Ruslan V. Maltsev (mrv.work.box@yandex.ru)
## @version    0.1
## @date       2020-
## @copyright  GNU Public License
##
# ----------------------------------------------------------------------------------------------------------
import os
#
import vs.make
import cmdArgs
# ----------------------------------------------------------------------------------------------------------

class multiMaker :
   
   def __init__ ( self, projectsDataPath_p, solutionStructurePath_p, groupNumber_p ) :
      self.__errors = []
      self.__projectsData = None
      self.__solutionStructure = None
      self.__projectsData = self.__loadDataFromJson( projectsDataPath_p, "projects list data" )
      if not self.__projectsData :
         return
      self.__solutionStructure = self.__loadDataFromJson( solutionStructurePath_p, "projects structure" )
      if not self.__solutionStructure :
         return
      self.__vsPaths = vs.make.vsPaths()
      if not self.__vsPaths.isValid() :
         if self.__vsPaths.errorsValues.installDirrectoryNotFound == vsPaths_l.errors() :
            self.__errors.append( '[ ERROR ] can not find Visual Studio install directory' )
            return 
         elif self.__vsPaths.errorsValues.buildUtilityDoesNotExists == vsPaths_l.errors() :
            self.__errors.append( '[ ERROR ] file "devenv.com" does not exists.'  )
            return       
      self.__groupNumber = groupNumber_p
   
   def errors ( self ) :
      return self.__errors

   def isValid ( self ) :   
      return not self.__errors
   
   def startMultiMaking ( self ) :
      if not self.isValid() :
         return
      solutionDirnameFieldName_l = "surname"
      for groupNumber in [ self.__groupNumber ] if self.__groupNumber else self.__projectsData.keys() :
         currentGourp_l = self.__projectsData.get( groupNumber )
         if currentGourp_l :
            for currentSolutionInfo in currentGourp_l :
               currentSolutionDirname_l = currentSolutionInfo.get( solutionDirnameFieldName_l )
               if os.path.exists( currentSolutionDirname_l ) :
                  os.chdir( currentSolutionDirname_l )
                  projectsForMaking_l = vs.make.labDescriptions( self.__solutionStructure ).formProjectsForMaking()
                  print( '*' * 21 )
                  print( '\n[ * ] solution "' + currentSolutionDirname_l + '"' )
                  print( '[ * ] cleaning directories ...' )
                  cleanedPathsSet_l = set()
                  for nextProject in projectsForMaking_l:
                     if nextProject.solutionPath() not in cleanedPathsSet_l :
                        nextProject.cleanLabProjectDirrectory()
                        cleanedPathsSet_l.add( nextProject.solutionPath() )
                  print( '[ * ] projects building...' )
                  for nextProject in projectsForMaking_l:
                     nextProject.build( self.__vsPaths.buildUtilityPath() )                  
                  os.chdir( ".." )
         
   def __loadDataFromJson( self, filePath_p, errorTxt_p ) :
      result_l = vs.make.labDescriptions.loadDataFromJson( filePath_p, errorTxt_p )
      if isinstance( result_l, str ) :
         self.__errors.append( result_l )
         return None
      return result_l

if "__main__" == __name__ :
   cmdArgumentsDescriptions_l = []
   cmdArgumentsDescriptions_l.append( cmdArgs.description( [ "-h", "--help" ], "help information" ) )
   cmdArgumentsDescriptions_l.append( cmdArgs.description( [ "-l", "--projects-list" ], 
      "path for configuration file with projects list", "" ) )
   cmdArgumentsDescriptions_l.append( cmdArgs.description( [ "-s", "--solution-structure" ], 
      "path for configuration file with solution structure description", "" ) )
   cmdArgumentsDescriptions_l.append( cmdArgs.description( [ "-n", "--group-id" ], 
      "making projects group id from configuration file (all groups on default)", "" ) )
   cmdArguments_l = cmdArgs.arguments( cmdArgumentsDescriptions_l )
   if cmdArguments_l.value( "--help" ) :
      print( "\nUtility for making MS VS projects determined in JSON configuration file\n" )
      print( cmdArguments_l.helpTxt() )
      exit( 0 )
   #
   multiMaker_l = multiMaker( cmdArguments_l.value( "--projects-list" ), cmdArguments_l.value( "--solution-structure" ), 
      cmdArguments_l.value( "--group-id" ) )
   if not multiMaker_l.isValid() :
      for next in multiMaker_l.errors() :
         print( next )
      exit( - len( multiMaker_l.errors() ) )
   multiMaker_l.startMultiMaking()