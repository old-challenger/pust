import sys
import math

__author__ = 'Ruslan V. Maltsev'

if 2 != len( sys.argv ):
   print( "\n[ERROR] Set hours amount in one (1) argument" )
   exit( -1 )

lectionsFactor = 1.25
practicsFactor = 1.5
oneLessonHours = float( 2 )

allHours = float( sys.argv[ 1 ] )
lectionsHours = math.floor( allHours / ( 1.25 + 1.5 ) )
if lectionsHours % 2:
   lectionsHours -= 1

while ( lectionsHours ):
   practicsHours = ( allHours - lectionsFactor * lectionsHours ) / practicsFactor
   print( "\n lections hours : ", lectionsHours, "; practics hours: ", practicsHours )
   lectionsHours -= oneLessonHours


