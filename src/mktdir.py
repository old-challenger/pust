## Utility which makes new temp directory with the name of current date and fomat yyyy-mm-dd.
## @package    mktdir 
## @author     Ruslan V. Maltsev (mrv.work.box@yandex.ru)
## @version    0.2
## @date       2013-
## @copyright  GNU Public License
## 
## This program makes new temp directory with the name of current date in format [prefix]yyyy-mm-dd[suffix]
## <ul>
## <li>yyyy - the year from the Nativity of Jesus Christ;
## <li>mm - the month number [01, 12];
## <li>dd - the day of the month [01, 31].
## </ul>
##
# ----------------------------------------------------------------------------------------------------------
import os
import sys
import time
import datetime
#
import cmdArgs
# ----------------------------------------------------------------------------------------------------------
cmdArgumentsDescriptions_g = []
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-h", "--help" ], "help information" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-p", "--prefix"], 
   "creating dirrectory name prefix", "" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-s", "--suffix"], 
   "creating dirrectory name suffix", "" ) )
cmdArguments_g = cmdArgs.arguments( cmdArgumentsDescriptions_g )
# checking incorrect argumets existing
unknownArgumnets_g = cmdArguments_g.unknown()
if None != unknownArgumnets_g:
   print( "\n[ERROR] " + unknownArgumnets_g )
   exit( 1 )
# checking help-parameter defined
if cmdArguments_g.value( "--help" ):
   print(   "\nThis program makes new temp directory with the name of current date in format:" +
            "\n\t[prefix]yyyy-mm-dd[suffix],\n" +  
            "\nyyyy - the year from the Nativity of Jesus Christ;" + 
            "\nmm - the month number [01, 12];" + 
            "\ndd - the day of the month [01, 31].\n" )
   print( cmdArguments_g.helpTxt() )
   exit( 0 )

currentDate_g = datetime.date.today().isoformat() 
dirName_g = cmdArguments_g.value( "--prefix" ) + currentDate_g + cmdArguments_g.value( "--suffix" )
if os.path.exists( dirName_g ):
   print ( "\n[WARNING] Directory \"" + dirName_g + "\" already exists.\n" )
   time.sleep( 1 )
else:
   os.mkdir( dirName_g )
# ----------------------------------------------------------------------------------------------------------   

