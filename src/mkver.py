## Utility which ranames or makes new file, contains in the name the date of last modifining.
## @package    mkver 
## @author     Ruslan V. Maltsev (mrv.work.box@yandex.ru)
## @version    0.2
## @date       2013-
## @copyright  GNU Public License
## 
## Command prompt arguments list:
## <ul>
## <li>-h - help information;
## <li>-f - initial file name;
## <li>-p - string which inserting between initial file name and date of last modifining;
## <li>-m - remove file after coping;
## <li>-o - open created file;
## <li>-fd - take the file modification date (not now date).
## </ul>
##
# ----------------------------------------------------------------------------------------------------------
import io
import os
import sys
import time
import datetime
import distutils
import distutils.file_util
#
import cmdArgs
# ----------------------------------------------------------------------------------------------------------

# configuring CMD argumets list
cmdArgumentsDescriptions_g = []
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-h", "--help" ], "help information" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-f", "--file"], "initial file name", "" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-p", "--date-prefix"], 
   "string which inserting between initial file name and date of last modifining", "." ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-m", "--move" ], 
   "move file (not copy)" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-o", "--open" ], 
   "open file after coping(moving)" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-fd", "--file-date" ], 
   "take the file modification date (not now date)" ) )
cmdArguments_g = cmdArgs.arguments( cmdArgumentsDescriptions_g )
# checking incorrect argumets existing
unknownArgumnets_g = cmdArguments_g.unknown()
if None != unknownArgumnets_g:
   print( "\n[ERROR] " + unknownArgumnets_g )
   exit( 1 )
# checking help-parameter defined
if cmdArguments_g.value( "--help" ):
   print(   "\nThis program ranames or makes copy of file, contains in his name the now date or date of last modification." +
            "\nNew file name has flowing format:" + 
            "\n\t<old file name>[date prefix]yyyy-mm-dd.<file extention>,\n" +  
            "\nyyyy - the year from the Nativity of Jesus Christ;" + 
            "\nmm - the month number [01, 12];" + 
            "\ndd - the day of the month [01, 31].\n" )
   print( cmdArguments_g.helpTxt() )
   exit( 0 )
#checking correct set of initial file name
fileName_g = cmdArguments_g.value( "--file" )
if not fileName_g:
   print( "\n[ERROR] Initial file name is not defined." )
   exit( 2 )
#creating new file name
if cmdArguments_g.value( "--file-date" ):
   dateString_g = time.strftime( "%Y-%m-%d", time.localtime( os.path.getmtime( fileName_g ) ) )
else:
   dateString_g = datetime.date.today().isoformat()
splittedFileName_g = os.path.splitext( fileName_g )
newFileName_g = ""
for namePart in splittedFileName_g[:-1]:
   newFileName_g += namePart 
newFileName_g += cmdArguments_g.value( "--date-prefix" ) + dateString_g + splittedFileName_g[ -1 ]
assert( newFileName_g )
#operating file
if os.path.exists( newFileName_g):
   print ( "\n[WARNING] File \"" + newFileName_g + "\" already exists.\n" )
else:
   if os.path.exists( fileName_g ):
      if cmdArguments_g.value( "--move" ):
         os.rename( fileName_g, newFileName_g )
      else:
         distutils.file_util.copy_file( fileName_g, newFileName_g )
   else:
      newFileObj_l = io.open( newFileName_g, "w" )
      newFileObj_l.close()
if cmdArguments_g.value( "--open" ):
   os.startfile( newFileName_g, "open" )
# ----------------------------------------------------------------------------------------------------------