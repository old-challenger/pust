## Utility for operating with files meeting by conditions expressed in templates or regular expressios.
## @package    rm 
## @author     Ruslan V. Maltsev (mrv.work.box@yandex.ru)
## @version    0.2
## @date       2013-
## @copyright  GNU Public License
##
# ----------------------------------------------------------------------------------------------------------
import re
import os
import sys
import time
import fnmatch
#
import cmdArgs
# ----------------------------------------------------------------------------------------------------------

## @brief Class for checking matching files names to list of regular expressions and files templates
class filesFilterClass:

   def __init__( self, templatesList_p = [], regExpList_p = [] ):
      self.__templatesList, self.__regExpList = templatesList_p, regExpList_p

   def isEmpty ( self ):
      return not len( self.__regExpList ) and not len( self.__templatesList )

   def addRegExp ( self, regExp_p ):
      try:
         testRegExp = re.compile( regExp_p )
      except :
         print ( "[Warning] Incorrect regular expression \"" + regExp_p + "\".\n" )
         return
      self.__regExpList.append( testRegExp )  
      
   def addFilesTemplate ( self, filesTemplate_p ):
      self.__templatesList.append( filesTemplate_p )

   def addCondition ( self, condition_p, isRegExp_p ):
      if isRegExp_p:
         self.addRegExp( condition_p )
      else:
         self.addFilesTemplate( condition_p ) 
      
   def checkName ( self, fileName_p ):
      for nextRegExp in self.__regExpList:
         if nextRegExp.match( fileName_p ):
            return nextRegExp.pattern
      for nextFileTemplate in self.__templatesList:
         if fnmatch.fnmatch( fileName_p, nextFileTemplate ):
            return nextFileTemplate
      return ""
#/class filesFilterClass:
   
def recurceDelete ( path_p, filter_p, useRecurcion_p, delDirs_p, delFiles_p ):
   for nextDir in os.listdir( path_p ):
      checkResult_l = filter_p.checkName( nextDir )
      fullCurrentPath_l = path_p + os.sep + nextDir
      currentPathIsADir_l = os.path.isdir( fullCurrentPath_l )
      if len( checkResult_l ):         
         outText_l = "* Name \"" + nextDir + "\" in \"" + fullCurrentPath_l + "\" path is matched to \"" \
            + checkResult_l + "\" expression."
         if currentPathIsADir_l:
            if delDirs_p:
               recurceDelete( fullCurrentPath_l, filesFilterClass( [], [ re.compile( ".*" ) ] ), True, 
                              delDirs_p, delFiles_p )
               outText_l += "\n* Directory \"" + nextDir + "\" was removed." 
               os.rmdir( fullCurrentPath_l )
               currentPathIsADir_l = False
         elif  os.path.isfile( fullCurrentPath_l ):
            if delFiles_p:
               outText_l += "\n* File \"" + fullCurrentPath_l + "\" was removed." 
               os.remove( fullCurrentPath_l )
         print( outText_l )
      if  useRecurcion_p and currentPathIsADir_l:
         recurceDelete( fullCurrentPath_l, filter_p, useRecurcion_p, delDirs_p, delFiles_p )
         
def printErrorMessageAndExit ( errorTxt_p, errorCode_p ):
   print( "\n[ERROR] " + errorTxt_p + "\n" )
   time.sleep( 1 )
   exit( errorCode_p )
# ----------------------------------------------------------------------------------------------------------
cmdArgumentsDescriptions_g = []
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-h", "--help" ], "help information" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-f", "--operate-files"], "operate files" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-d", "--operate-directories"], 
   "operate directories" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-a", "--operate-files-and-directories"], 
   "operate files and directories" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-r", "--use-recursion" ], 
   "use recursion in operations" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-t", "--use-templates" ], 
   "use files names templates for operations conditions" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-e", "--use-regexp" ], 
   "use regular expressions for operations conditions" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-f", "--conditions-file"], 
   "get conditions from file", "" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-c", "--remove-condition"], 
   "set one simple condition", "" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-s", "--source-dirrectory"], 
   "set root dirrectory for start operations", "." ) )
cmdArguments_g = cmdArgs.arguments( cmdArgumentsDescriptions_g )
# checking help-parameter defined
if cmdArguments_g.value( "--help" ):
   print( "\nUtility for removing files meeting by conditions expressed in templates or regular expressios.\n" )
   print( cmdArguments_g.helpTxt() )
   exit( 0 )
# checking source dirrectory path
sourceDir_g = cmdArguments_g.value( "--source-dirrectory" )
assert( len( sourceDir_g ) ) 
if not os.path.exists( sourceDir_g ):
   printErrorMessageAndExit( "Path \"" + sourceDir_g + "\" doesn't exists.", 1 )
if not os.path.isdir( sourceDir_g ):
   printErrorMessageAndExit( "\"" + sourceDir_g + "\" isn't a dirrectory.", 2 )
# filling files filter
filesFilter_g = filesFilterClass()
useRegExpr_g = cmdArguments_g.value( "--use-regexp" )
for condition in cmdArguments_g.unknownList():
   filesFilter_g.addCondition( condition, useRegExpr_g )
conditionsFileName_g = cmdArguments_g.value( "--conditions-file" )
# adding conditions from file
if len( conditionsFileName_g ):
   if not os.path.exists( conditionsFileName_g ):
      printErrorMessageAndExit( "Path \"" + conditionsFileName_g + "\" doesn't exists.", 3 )
   if not os.path.isfile( conditionsFileName_g ):
      printErrorMessageAndExit( "Path \"" + conditionsFileName_g + "\" isn't a file.", 4 )
   # reading file with conditions list
   conditionsFileObj_l = open( conditionsFileName_g, "r" )
   nextLine_l = conditionsFileObj_l.readline()
   while len( nextLine_l ):
      stripedLine_l = nextLine_l.strip( " \n\t" )
      filesFilter_g.addCondition( stripedLine_l, useRegExpr_g )
      nextLine_l = conditionsFileObj_l.readline()
   conditionsFileObj_l.close()
simgleCondition_g = cmdArguments_g.value( "--remove-condition" )
if len( simgleCondition_g ):
   filesFilter_g.addCondition( simgleCondition_g, useRegExpr_g )
# start operations
if filesFilter_g.isEmpty():
   printErrorMessageAndExit( "Operating conditions isn't defined.", 5 )
if cmdArguments_g.value( "--operate-files-and-directories" ):
   delFiles_g, delDirs_g = True, True
else:
   delFiles_g = cmdArguments_g.value( "--operate-files" ) 
   delDirs_g = cmdArguments_g.value( "--operate-directories" )
recurceDelete( os.path.abspath( sourceDir_g ), filesFilter_g, cmdArguments_g.value( "--use-recursion" ), 
   delDirs_g, delFiles_g )
# ----------------------------------------------------------------------------------------------------------
