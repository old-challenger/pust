# ----------------------------------------------------------------------------------------------------------
__author__ = 'Ruslan V. Maltsev'
""" Интерфейс запуска 
   roirun 1 2 3 5 
   
   Указываются номера лабораторных работ или названия решений (solution), для которых необходимо выполнить 
   сборку. Вызов программы без параметров выполняет сборку всех проектов.
   
   Текущая версия выполняет сборку следующих проектов:
      generator      - лабораторная работа №1 ( параметр 1 или generator )
      monothread     - лабораторная работа №2 ( параметр 2 или monothread )
      multithread    - лабораторная работа №3 ( параметр 3 или multithread )
      mapreduce      - лабораторная работа №5 ( параметр 5 или mapreduce )
"""
# ----------------------------------------------------------------------------------------------------------
import cmdArgs
import vsmake

import io
import os
import sys
import shutil
import random
import subprocess
# ----------------------------------------------------------------------------------------------------------

def run ( projectName_p, params_p ):
   sys.stdout.write( '\n ------- "' + projectName_p + '" program running... ----------- ' )   
   runningFile_l = projectName_p + ".exe"
   if not os.path.exists( runningFile_l ):
      sys.stdout.write( '\n[ ERROR ] Required executable file "' + runningFile_l + '" does not exists.' )
      return
   allParams_l = [ runningFile_l ] + params_p
   runningString_l = str()
   for next in allParams_l:
      runningString_l += next + ' '
   sys.stdout.write( '\n * Running command: \n\t' + runningString_l )
   subprocess.call( allParams_l )
   sys.stdout.write( '\n ------- "' + projectName_p + '" program finished. ----------- ' )
   sys.stdout.flush()

def smartRun ( projectName_p, imperativeParamsList_p, aditionalParamsList_p ):
   assert( isinstance( imperativeParamsList_p, list ) )
   assert( isinstance( aditionalParamsList_p, list ) )
   imperativeParamsSet_l = imperativeParamsList_p
   run( projectName_p, imperativeParamsList_p 
      + [ next for next in aditionalParamsList_p if not next in imperativeParamsSet_l ] )
      
def getNextText ( increaseCounter_p, outputDir_p ) :
   sys.stdout.write( '\n * Getting next text file for encryption ... ' )
   numberFileName_l = "..\..\\..\\..\\number.inf"
   if not os.path.exists( numberFileName_l ) :
      fileNumber_l = 0
      with open( numberFileName_l, "w" ) as fileObj_l : 
         fileObj_l.write( str( fileNumber_l ) )
   else :
      with open( numberFileName_l, "r" ) as fileObj_l : 
         fileNumber_l = int( fileObj_l.read() )
      if increaseCounter_p :
         with open( numberFileName_l, "w" ) as fileObj_l : 
            fileNumber_l += 1
            fileObj_l.write( str( fileNumber_l ) )
   textFileName_l = "..\\..\\..\\..\\txt4crypt\\" + str( fileNumber_l ).rjust( 2, "0" ) + ".txt"
   if not os.path.exists( textFileName_l ) :
      return False
   sys.stdout.write( '[ OK ]' )
   sys.stdout.write( '\n * Coping file "%s" to work dirrectory ...' % textFileName_l )
   shutil.copyfile( textFileName_l, outputDir_p + "text.txt" )
   sys.stdout.write( '[ OK ]' )
   sys.stdout.flush()
   return True

def generatePasswd ( simbolsVariantsAmount_p, minLength_p, maxLength_p ) :
   #incorrectSymbolsSet_l = { '|' } 
   result_l = str()
   passwdSize_l = random.randint( minLength_p, maxLength_p )
   for i in range( passwdSize_l ) :
      randomValue_l = random.randint( 0, simbolsVariantsAmount_p )
      nextChar_l = chr( ord( '0' ) + randomValue_l ) 
      #if nextChar_l not in incorrectSymbolsSet_l :
      result_l += nextChar_l
   return result_l
   
def generatePasswords ( big_p, small_p, outputDir_p ) :
   sys.stdout.write( '\n * Generating password for encryption ... ' )
   digitsAmount_l = 10
   alphaAmount_l = 27
   signsAmount_l = 13
   passwd_l = big_p if big_p else generatePasswd( digitsAmount_l + alphaAmount_l * 2 + signsAmount_l - 1, 10, 13 )
   with open( outputDir_p + "passwd.txt", "w" ) as fileObj_l : 
      fileObj_l.write( passwd_l )
   smallPasswd_l = small_p if small_p else generatePasswd( digitsAmount_l - 1, 5, 5 )
   return smallPasswd_l, passwd_l
   sys.stdout.write( '[ OK ]' )
   sys.stdout.flush()
   
def createZip ( passwd_p, outputDir_p ):
   sys.stdout.write( '\n * ZIP-archive creation ... ' )
   projectsForMaking = vsmake.labDescriptions().formProjectsForMaking( [] )
   imperativeParamsMap = { "encrypt" : 
      [ "–p", '"%s"' % passwd_p, "-o", outputDir_p + "encrypted.zip", outputDir_p + "text.txt" ] }
   for next in projectsForMaking:
      smartRun( next.projectName(), imperativeParamsMap[ next.projectName() ], [] )

def createMessage( passwords_p, outputDir_p ) :   
   sys.stdout.write( '\n * Crypted message creation ... ' )
   scriptPath_l = "..\\script\\encrypt.py"
   command_l = 'python %s -p %s "%s" > %smessage.txt' % ( ( scriptPath_l, ) + passwords_p + ( outputDir_p, ) )
   sys.stdout.write( '\n * Runing shell-command: \n\t%s\n' % command_l )
   os.system( command_l )
   shutil.copyfile( scriptPath_l, outputDir_p + "encrypt.py" )
   sys.stdout.flush()
   
def createOutputDir ( outputDir_p ) :
   print( "!:" + outputDir_p )
   if not os.path.exists( outputDir_p ) :
      sys.stdout.write( '\n * Output dirrectory creation ... ' )
      os.mkdir ( outputDir_p )
      sys.stdout.write( '[ OK ]' )
      sys.stdout.flush()
   elif not os.path.isdir( outputDir_p ) :
      sys.stdout.write( '\n [ERROR] Can\'t create output dirrectory "%s"' % outputDir_p )
      return None
   return outputDir_p
      
sys.stdout.write( "\n ------------- Crypto labwork project operations running ------------- " )
cmdArgumentsDescriptions_g = []
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-h", "--help" ], "help information" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-bp", "--big-passwd" ], 
   "the big password", "" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-sp", "--small-passwd" ], 
   "the small password", "" ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-o", "--output" ], 
   "output dirrectory", "." ) )
cmdArgumentsDescriptions_g.append( cmdArgs.description( [ "-i", "--increase-counter" ], "increase counter" ) )
cmdArguments_g = cmdArgs.arguments( cmdArgumentsDescriptions_g )
exitCode = 0
if cmdArguments_g.value( "--help" ):
   sys.stdout.write( "\n" + cmdArguments_g.helpTxt() )
else : 
   outputDir = createOutputDir( cmdArguments_g.value( "--output" ) + "\\task\\" )
   if not outputDir :
      exitCode = 1
   elif not getNextText( cmdArguments_g.value( "--increase-counter" ), outputDir ) :
      exitCode = 2
   else :
      passwords = generatePasswords( cmdArguments_g.value( "--big-passwd" ), cmdArguments_g.value( "--small-passwd" ), outputDir )
      createZip( passwords[ 1 ], outputDir )
      createMessage( passwords, outputDir )
sys.stdout.write( "\n ------------- Crypto labwork project operations finished. ----------- " )
sys.stdout.flush()
exit( exitCode )


