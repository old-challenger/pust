## utility for spliting output environment variable values
## @package    split.envar 
## @author     Ruslan V. Maltsev (mrv.work.box@yandex.ru)
## @version    0.2
## @date       2013-
## @copyright  GNU Public License
##
# ----------------------------------------------------------------------------------------------------------
import os
import sys
# ----------------------------------------------------------------------------------------------------------
if 1 < len( sys.argv ):
   variableForSplit = sys.argv[ 1 ]
else:
   variableForSplit = 'PATH'

if 2 < len( sys.argv ):
   delimeter = sys.argv[ 2 ]
else:
   delimeter = ';'
   
for singlePath in os.environ[ variableForSplit ].split( delimeter ):
   print( singlePath )
