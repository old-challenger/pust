## Group setting of environment variables values in MS Windows registry
## @package    winregKeys 
## @author     Ruslan V. Maltsev (mrv.work.box@yandex.ru)
## @version    0.3
## @date       2015-
## @copyright  GNU Public License
##
## Интерфейс запуска: 
## winregKeys -h | --help
##  winregKeys set|get [ -o | --output-file <filename> ] [ -i | --input-file <filename> ] variable1 variable2 ...
## <ul>
## <li>set - задать значения переменных
## <li>get - получить значения переменных (указанные значения переменных и их типы игнорируются)
##    <ul>
##    <li>-o или --output-file - определить имя файла со списком переменных
##    <li>-i или --input-file - определить имя файла для вывода результата запроса
##    </ul>
## <li>формат задания значений переменных: имя(тип)=значение
## </ul>
##
# ----------------------------------------------------------------------------------------------------------
import sys
import winreg
import cmdArgs
# ----------------------------------------------------------------------------------------------------------

## @brief Registry variable description
class variable :

   def name ( self ) :
      return self.__name

   def type ( self ) :
      return self.__type

   def value ( self ) :
      return self.__value

   def __init__ ( self, name_p, type_p, value_p ) :
       self.__name = name_p
       self.__type = type_p 
       self.__value = value_p 

   def __str__ ( self ) :
       return "name : " + self.__name + "; type : " + str( self.__type ) + "; value : " + str( self.__value )

## @brief Application main class
class application :

   def code2Txt ( self, code_p ) :
      return self.__outputTxtDict.get( code_p )

   def helpTxt ( self ) :
      return """Windows environment variables configurator.
   winregKeys -h | --help
   winregKeys set | get [ -o | --output-file <filename> ] [ -i | --input-file <filename> ] variable1 variable2 ... 

      set - set variables values
      get - get variables values (determined variables values and types will be ignored)

      variables config format (default type is REG_SZ):
         name(type)=value
   \n""" + self.__cmdArguments.helpTxt()
   
   def operate ( self ) :
      if 2 > len( sys.argv ) :
         return -1
      self.__cmdArguments = cmdArgs.arguments( self.__cmdArgumentsDescriptions )
      if self.__cmdArguments.value( "--help" ) :
         return 1
      self.__openRegistryKey()
      if not self.__registryKey :
         return -2
      self.__formVariablesDict()
      subcommand_l = sys.argv[ 1 ].lower()
      if "set" == subcommand_l :
         return self.__set()
      elif "get" == subcommand_l :
         return self.__get()
      else :
         return -3
      
   def __init__ ( self ) :
      self.__cmdArgumentsDescriptions = [
         cmdArgs.description( [ "-h", "--help" ], "help information" ),
         cmdArgs.description( [ "-i", "--input-file" ], "input config file name", "" ),
         cmdArgs.description( [ "-o", "--output-file" ], "output file name", "" ),
         cmdArgs.description( [ "-b", "--registry-branch" ], "target registry branch", "HKEY_LOCAL_MACHINE" ) 
         ]
      self.__registryKeyName = "SYSTEM\CurrentControlSet\Control\Session Manager\Environment"
      self.__branchesDict = {
         "HKEY_CLASSES_ROOT"     : winreg.HKEY_CLASSES_ROOT,
         "HKEY_CURRENT_USER"     : winreg.HKEY_CURRENT_USER,
         "HKEY_LOCAL_MACHINE"    : winreg.HKEY_LOCAL_MACHINE, 
         "HKEY_USERS"            : winreg.HKEY_USERS,
         "HKEY_PERFORMANCE_DATA" : winreg.HKEY_PERFORMANCE_DATA,
         "HKEY_CURRENT_CONFIG"   : winreg.HKEY_CURRENT_CONFIG 
         }
      self.__typesDict = {
         "REG_BINARY"      : winreg.REG_BINARY,
         "REG_DWORD"       : winreg.REG_DWORD,
         "REG_EXPAND_SZ"   : winreg.REG_EXPAND_SZ,
         "REG_LINK"        : winreg.REG_LINK,
         "REG_SZ"          : winreg.REG_SZ 
         }
      self.__filesEncoding = "utf-8"
      self.__outputTxtDict = {
         0  : "Ok",
         -1 : "\n[ERROR] No commandline arguments.",
         -2 : "\n[ERROR] Can not open registry key \"" + self.__registryKeyName + "\".",
         -3 : "\n[ERROR] Incorrect sub command (must be set or get).",
         -4 : "\n[ERROR] Can not open output file."
         }

   def __parceVariableDefinition ( self, line_p ) :
      assignmentList_l = line_p.split( "=" )
      listLen_l = len( assignmentList_l )
      if listLen_l > 2 :
         return None
      typedname_l = assignmentList_l[ 0 ].strip() 
      value_l = assignmentList_l[ 1 ].strip() if listLen_l > 1 else None
      typednameList_l = typedname_l.split( "(" )
      listLen_l = len( typednameList_l )
      if listLen_l > 2 or ( 2 == listLen_l and ")" != typednameList_l[ 1 ][-1] ) :
         return None
      name_l = typednameList_l[ 0 ].strip() 
      type_l = typednameList_l[ 1 ][ : -1 ].strip() if listLen_l > 1 else None
      return variable( name_l, type_l, value_l )

   def __formVariablesDict ( self ) :
      self.__variables = dict() 
      variablesFromCommandLine_l = self.__cmdArguments.unknownList()
      assert( "set" == variablesFromCommandLine_l[ 0 ] or "get" == variablesFromCommandLine_l[ 0 ] )
      for next in variablesFromCommandLine_l[ 1 : ]:
         variable_l = self.__parceVariableDefinition( next )
         self.__variables[ variable_l.name() ] = variable_l
      inFileName_l = self.__cmdArguments.value( "--input-file" )
      if inFileName_l :
         inFile_l = open( inFileName_l, "r", encoding = self.__filesEncoding )
         if inFile_l :
            nextLine_l = inFile_l.readline().strip()
            while nextLine_l :
               variable_l = self.__parceVariableDefinition( nextLine_l )
               self.__variables[ variable_l.name() ] = variable_l
               nextLine_l = inFile_l.readline()
            inFile_l.close()

   def __openRegistryKey ( self ) :
      branch_l = self.__branchesDict.get( self.__cmdArguments.value( "--registry-branch" ) )
      if branch_l :
         self.__registryKey = winreg.OpenKey( branch_l, self.__registryKeyName, 0, winreg.KEY_SET_VALUE | winreg.KEY_READ )
      else :
         self.__registryKey = None

   def __set ( self ) :
      for nextVariable in self.__variables.values() :
         if None != nextVariable.type() and None != nextVariable.value() :
            found_l = self.__typesDict.get( nextVariable.type() )
            if found_l :
               winreg.SetValueEx( self.__registryKey, nextVariable.name(), 0, found_l, nextVariable.value() ) 
      return 0

   def __get ( self ) :
      outFileName_l = self.__cmdArguments.value( "--output-file" )
      if outFileName_l :
         out_l = open( outFileName_l, "w", encoding = self.__filesEncoding )
         if not out_l :
            return -4
      else :
         out_l = sys.stdout
      for nextVariable in self.__variables.values() :
         queryResult_l = winreg.QueryValueEx( self.__registryKey, nextVariable.name() ) 
         out_l.write( nextVariable.name() + " = \"" + queryResult_l[ 0 ] + "\"\n" )
      if outFileName_l :
         out_l.close()
      else :
         out_l.flush()
      return 0

application_g = application()
resultCode_g = application_g.operate()
if 0 > resultCode_g :
   sys.stdout.write( application_g.code2Txt( resultCode_g ) )
elif 1 == resultCode_g :
   sys.stdout.write( application_g.helpTxt() )
sys.stdout.write( "\n* Programm fibished." )
sys.stdout.flush()
exit( resultCode_g if 0 > resultCode_g else 0 )