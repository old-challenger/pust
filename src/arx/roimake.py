# ----------------------------------------------------------------------------------------------------------
__author__ = 'Ruslan V. Maltsev'
""" Интерфейс запуска 
   roimake 1 2 3 5
   
   Указываются номера лабораторных работ или названия решений (solution), для которых необходимо выполнить 
   сборку. Вызов программы без параметров выполняет сборку всех проектов.
   
   Текущая версия выполняет сборку следующих проектов:
      generator      - лабораторная работа №1 ( параметр 1 или generator )
      monothread     - лабораторная работа №2 ( параметр 2 или monothread )
      multithread    - лабораторная работа №3 ( параметр 3 или multithread )
      mapreduce      - лабораторная работа №5 ( параметр 5 или mapreduce )
"""
# ----------------------------------------------------------------------------------------------------------
import os
import re
import sys
import winreg
import subprocess
import xml.etree.ElementTree
# ----------------------------------------------------------------------------------------------------------

class xmlCheckingElementValues :
   
   class regexDescriptor :

      def __init__ ( self, id_p ) :
         self.__reDescriptor = id_p

      def __eq__ ( self, value_p ) :
         return bool( self.__reDescriptor.match( value_p ) )

   def __init__ ( self, name_p, label_p = None, value_p = None, namespace_p = "", 
                        autocorrection_p = False, required_p = False ) :
      self.__autocorrection = autocorrection_p
      self.__childs = list()
      self.__label = label_p
      self.__namespace = namespace_p
      self.__name = name_p
      self.__required = required_p
      self.__value = value_p if not value_p or isinstance( value_p, str ) \
                        else xmlCheckingElementValues.regexDescriptor( value_p )
   
   def addChild ( self, element_p ) :
      self.__childs.append( element_p )
      
   def autocorrection ( self ) :
      return self.__autocorrection
      
   def childs ( self ) :
      return self.__childs
   
   def formNamespace ( self, fullName_p ) :
      nameLen_l = len( self.__name )
      assert( self.__name == fullName_p[ -nameLen_l : ] )
      self.__namespace = fullName_p[ : -nameLen_l ]
      return self.__namespace
   
   def name ( self ) :
      return self.__namespace + self.__name
      
   def namespace ( self ) :
      return self.__namespace
   
   def label ( self ) :
      return self.__label
   
   def rawName ( self ) :
      return self.__name
      
   def required ( self ) :
      return self.__required
   
   def value ( self ) :
      return self.__value
   
   def __eq__ ( self, element_p ) :
      value_l = element_p.get( self.__label ) if self.__label else element_p.text
      return value_l and value_l == self.__value

class vsProjectOperator :
   # названия проектов по номерам лабораторных работ
   """ __projectsNumbers2NamesMap = {  
      1  : "generator",
      2  : "monothread",
      3  : "multithread",
      5  : "mapreduce"
   } """
   
   __projectsNumbers2NamesMap = {  
      1  : ( "monothread", "generator" ),
      2  : "multithread",
      3  : "mapreduce"
   }
   
   __projectsNames2NumersMap = None
   
   __projectBuildConfiguration = 'release|x64' # "release"
   
   @staticmethod
   def formLabFolderName ( labNumber_p ) :
      return str( labNumber_p ).rjust( 2, "0" )
   
   @classmethod
   def formProjectFilePath ( cls, labNumber_p, projectName_p, solutionName_p = None) :
      return "..\\" + cls.formLabFolderName( labNumber_p ) + "\\src\\" +  \
         ( ( projectName_p + "\\" + projectName_p ) if solutionName_p else projectName_p ) + ".vcxproj"

   @classmethod
   def formSolutionFilePath ( cls, labNumber_p, solutionName_p ) :
      return "..\\" + cls.formLabFolderName( labNumber_p ) + "\\src\\" + solutionName_p + ".sln"
      
   @classmethod
   def formProjectsForMaking ( cls, cmdArguments_p ) :
      projectsDescriptors_l = cmdArguments_p if cmdArguments_p else list( cls.__projectsNumbers2NamesMap.values() )
      result_l = list()
      for i in projectsDescriptors_l :
         if isinstance( i, tuple ) :
            for j in i :
               vsProjectOperator.__addNextProjectOperator( j, result_l )
         else :
            vsProjectOperator.__addNextProjectOperator( i, result_l )
      return result_l 
   
   @staticmethod
   def incorrectArgumetPrint ( argumentTxtValue_p ) :
      sys.stdout.write( '\n[ WARNIG ] Icorrect argument value \"' + argumentTxtValue_p + '\"' )
      
   @classmethod
   def name2number ( cls, name_p ) :
      if not cls.__projectsNames2NumersMap :
         cls.__projectsNames2NumersMap = dict()
         for ( labNumber, names ) in cls.__projectsNumbers2NamesMap.items() :
            if isinstance( names, tuple ):
               for projectName in names :
                  cls.__projectsNames2NumersMap[ projectName ] = labNumber
            else :
               cls.__projectsNames2NumersMap[ names ] = labNumber
      return cls.__projectsNames2NumersMap.get( name_p )
      
   @classmethod
   def number2name ( cls, number_p ) :
      return cls.__projectsNumbers2NamesMap.get( number_p )
      
   def __init__ ( self, labNumber_p, solutionName_p, projectName_p ) :
      self.__labNumber = labNumber_p
      self.__projectName =  projectName_p
      self.__solutionName = solutionName_p

   def build( self, vsBuildUtilityPath_p ) :
      sys.stdout.write( '\n\t[ ** ] "' + self.__projectName + '" project building...' )
      solutionFilePath_l = self.formSolutionFilePath( self.__labNumber, self.__solutionName )
      if not os.path.exists( solutionFilePath_l ) :
         sys.stdout.write( "\n\t\t[ ERROR ] Required solution file does not exists: " )
         sys.stdout.write( '\n\t\t\t "' + solutionFilePath_l + '".' )
         return
      self.__projectFilePath = self.formProjectFilePath( self.__labNumber, self.__projectName )
      if not os.path.exists( self.__projectFilePath ) :
         self.__projectFilePath = self.formProjectFilePath( self.__labNumber, self.__projectName, self.__solutionName )
         if not os.path.exists( self.__projectFilePath ) :
            sys.stdout.write( "\n\t\t[ ERROR ] Required project file does not exists: " )
            sys.stdout.write( '\n\t\t\t "' + self.__projectFilePath + '".' )
            return
      sys.stdout.write( "\n\t\t[ *** ] MS VS output begin: \n" )
      sys.stdout.flush()
      if not self.__checkProjectFile() :
         subprocess.call( [ vsBuildUtilityPath_p, solutionFilePath_l, "/rebuild", self.__projectBuildConfiguration, \
            "/project", self.__projectName ] )
      sys.stdout.write( "\n\t\t[ *** ] MS VS output end.\n" )
      sys.stdout.flush()
   
   def cleanLabProjectDirrectory ( self ) :
      dirPath_l = "../" + self.formLabFolderName( self.__labNumber )
      subprocess.call( [ "cleansrc.cmd", dirPath_l ] )
      sys.stdout.write( "\n\t\"" + dirPath_l + "\" directory was cleaned;" )
      sys.stdout.flush()
   
   def isValid ( self ) :
      return None != self.__labName and None != self.__projectNumber and None != self.__solutionName
      
   """def labName ( self ) :
      return self.__labName """
   def projectName ( self ) :
      return self.__projectName
      
   def solutionName ( self ) :
      return self.__solutionName
      
   def labNumber ( self ) :
      return self.__labNumber
      
   @classmethod
   def __addNextProjectOperator ( cls, projectDescriptionStr_p, operatorsList_p ) :
      if projectDescriptionStr_p.isdigit() :
         labNumber_l = int( projectDescriptionStr_p )
         names_l = cls.__projectsNumbers2NamesMap.get( labNumber_l )
         if None == names_l :
            self.incorrectArgumetPrint( projectDescriptionStr_p )
         else :
            if isinstance( names_l, tuple ) :
               solutionName_l = names_l[ 0 ]
               for projectName in names_l :
                  operatorsList_p.append( vsProjectOperator( labNumber_l, solutionName_l, projectName ) )
            else :
               operatorsList_p.append( vsProjectOperator( labNumber_l, names_l, names_l ) )
      elif projectDescriptionStr_p.isalpha() :
         projectName_l = projectDescriptionStr_p.lower()
         labNumber_l = cls.name2number( projectName_l )
         if None == labNumber_l :
            self.incorrectArgumetPrint( projectDescriptionStr_p )
         else :
            names_l = cls.__projectsNumbers2NamesMap.get( labNumber_l )
            solutionName_l = names_l[ 0 ] if isinstance( names_l, tuple ) else names_l
            operatorsList_p.append( vsProjectOperator( labNumber_l, solutionName_l, projectName_l ) )
      else:
         self.incorrectArgumetPrint( projectDescriptionStr_p )
   
   def __checkProjectFile( self ) :
      msNamespace_l = "http://schemas.microsoft.com/developer/msbuild/2003"
      root_l = xmlCheckingElementValues( "Project" )
      xml.etree.ElementTree.register_namespace( '', msNamespace_l )
      tree_l = xml.etree.ElementTree.parse( self.__projectFilePath )
      rootElement_l = tree_l.getroot()
      root_l.formNamespace( rootElement_l.tag )
      assert( "{%s}" % msNamespace_l == root_l.namespace() )
      globals_l = xmlCheckingElementValues( "PropertyGroup", "Label", "Globals", root_l.namespace() )
      globals_l.addChild( xmlCheckingElementValues(   "WindowsTargetPlatformVersion", value_p = "10.0.15063.0", 
                                                      namespace_p = root_l.namespace(), 
                                                      autocorrection_p = True, required_p = True ) )
      configurationRelease_l = xmlCheckingElementValues( "PropertyGroup", "Condition", 
         re.compile( "^'\$\(Configuration\)\|\$\(Platform\)'=='Release\|x64" ), root_l.namespace() )
      configurationDebug_l = xmlCheckingElementValues( "PropertyGroup", "Condition", 
         re.compile( "^'\$\(Configuration\)\|\$\(Platform\)'=='Debug\|x64" ), root_l.namespace() )
      configurationRelease_l.addChild( xmlCheckingElementValues(  "OutDir", value_p = "$(SolutionDir)..\\..\\test\\", 
                                                                  namespace_p = root_l.namespace() ) )
      configurationDebug_l.addChild( xmlCheckingElementValues( "OutDir", value_p = "$(SolutionDir)..\\..\\test\\debug\\", 
                                                               namespace_p = root_l.namespace() ) )
      intDir_l = xmlCheckingElementValues( "IntDir", 
         value_p = "$(OBJDIR)\\$(SolutionName)\\$(ProjectName)\\$(PlatformShortName)\\$(Configuration)\\", 
         namespace_p = root_l.namespace() )
      configurationRelease_l.addChild( intDir_l )
      configurationDebug_l.addChild( intDir_l )
      #
      result_l = 0
      writeCorrections2File_l = False
      checkingsRoot_l = ( globals_l, configurationRelease_l, configurationDebug_l ) 
      for rootElement in rootElement_l.findall( globals_l.name() ) :
         for rootChecker in checkingsRoot_l :
            if rootElement == rootChecker :
               for checker in rootChecker.childs() :
                  currentElement_l = rootElement.find( checker.name() )
                  if None == currentElement_l :
                     if checker.required() :
                        sys.stdout.write( "\n[ERROR] no tag <" + checker.name() + "> in \"" 
                           + self.__projectFilePath + "\" project file." )
                        result_l |= 0x1
                     continue
                  if currentElement_l != checker :
                     if checker.autocorrection() :
                        currentElement_l.text = checker.value()
                        sys.stdout.write( "\n[WARNING] Tag <" + checker.rawName() + "> text will be corrected in project file." )
                        writeCorrections2File_l = True
                     else :
                        sys.stdout.write( "\n[ERROR] Incorrect tag <" + checker.rawName() + "> value in \""
                           + self.__projectFilePath + "\" project file.\n\tMust be this: \"" + checker.value() + "\"" )
                        result_l |= 0x2
                  sys.stdout.flush() 
      if writeCorrections2File_l :
         tree_l.write( self.__projectFilePath, xml_declaration = True, encoding = "utf-8" )
      return result_l

class vsPaths :
   class errorsValues :
      noErrors                         = 0x0
      installDirrectoryNotFound        = 0x1
      installDirrectoryDoesNotExists   = 0x2
      buildUtilityDoesNotExists        = 0x4
   
   def __init__ ( self ) :
      self.__errors = self.errorsValues.noErrors
      self.__getVSpath()
      self.__getDevEnvPath()

   def errors( self ) :
      return self.__errors

   def isValid ( self ) :
      return self.errorsValues.noErrors == self.__errors
      
   def installDirrectoryPath ( self ) :
      return self.__installDirrectory
      
   def buildUtilityPath ( self ) :
      return self.__buildUtilityPath
      
   def __getVSpath ( self ):
      rootKey_l = winreg.HKEY_LOCAL_MACHINE #winreg.HKEY_CURRENT_USER
      regKeyPath_l = "SOFTWARE\\WOW6432Node\\Microsoft\\VisualStudio\\SxS\\VS7" #"Software\\Microsoft\\VisualStudio\\12.0_Config"
      regKeyName_l = "15.0" #"ShellFolder"
      sys.stdout.write( '\n[ * ] Searching Microsoft Visual Studio ...' )
      registryKey_l = winreg.OpenKey( rootKey_l, regKeyPath_l )
      self.__installDirrectory = winreg.QueryValueEx( registryKey_l, regKeyName_l )[ 0 ]
      sys.stdout.write( '\n\tInstall directory for Microsoft Visual Studio is: ' \
         + '\n\t\t"' + self.__installDirrectory + '"' )
      if not os.path.exists( self.__installDirrectory ):
         sys.stdout.write( "\n[ ERROR ] Microsoft Visual Studio install directory does not exists." )
         self.__errors |= self.errorsValues.installDirrectoryDoesNotExists

   def __getDevEnvPath ( self ):
      sys.stdout.write( '\n[ * ] Checking Microsoft Visual Studio development environmet existing...' )
      self.__buildUtilityPath = self.__installDirrectory + "Common7\\IDE\\devenv.com"
      if not os.path.exists( self.__buildUtilityPath ):
         sys.stdout.write( "\n[ ERROR ] Required executable file does not exists: " )
         sys.stdout.write( '\n\t "' + self.__buildUtilityPath + '".' )
         self.__errors |= self.errorsValues.buildUtilityDoesNotExists
      
if "__main__" == __name__ :
   minusesTxt_l = "-" * 13
   sys.stdout.write( "\n " + minusesTxt_l + " ROI labworks projects making programm " + minusesTxt_l )
   vsPaths_l = vsPaths()
   if not vsPaths_l.isValid() :
      errorCode_l = 0
      if vsPaths.errorsValues.installDirrectoryNotFound == vsPaths_l.errors() :
         sys.stdout.write( '\n[ ERROR ] Can not find Visual Studio install directory' )
         errorCode_l = -1
      elif vsPaths.errorsValues.buildUtilityDoesNotExists == vsPaths_l.errors() :
         sys.stdout.write( '\n[ ERROR ] File "devenv.com" does not exists.'  )
         errorCode_l = -2
      sys.stdout.flush()
      exit( errorCode_l )
   projectsForMaking = vsProjectOperator.formProjectsForMaking( sys.argv[ 1: ] )
   if not projectsForMaking:
      sys.stdout.write( '\n[ ERROR ] Making projects list is empty' )
      exit( -3 )
      sys.stdout.flush()      
   sys.stdout.write( "\n[ * ] Following projects will be compiled:" )
   for next in projectsForMaking:
      #sys.stdout.write( "!!! " + str( next.projectName() ) + str( next.solutionName() ) )
      sys.stdout.write( '\n\t project "' + next.projectName() + '"\tin solution "' + next.solutionName() \
         + '"\tfrom labwork №' + str( next.labNumber() ) )      
   sys.stdout.write( "\n[ * ] Cleaning directories ..." )
   sys.stdout.flush()
   for next in projectsForMaking:
      next.cleanLabProjectDirrectory()
   sys.stdout.write( "\n[ * ] Projects building..." )
   for next in projectsForMaking:
      next.build( vsPaths_l.buildUtilityPath() )