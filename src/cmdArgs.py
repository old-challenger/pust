## Module for working with cmd-arguments.
## @package    cmdArgs 
## @author     Ruslan V. Maltsev (mrv.work.box@yandex.ru)
## @version    0.2
## @date       2013-
## @copyright  GNU Public License
##
# ----------------------------------------------------------------------------------------------------------
import sys
# ----------------------------------------------------------------------------------------------------------
## @brief Class which includes description of cmd-arguments
class description:
   __keys = []
   __note = ""
   __defaultValue = ""
   __value = ""

   def __init__ ( self, keys_p, note_p, defaultValue_p = None ):
      self.__keys = keys_p[:]
      self.__note = note_p
      self.__defaultValue = defaultValue_p
      self.__value = self.__defaultValue

   def keys ( self ):
      return self.__keys

   def description ( self ):
      return self.__note

   def defaultValue ( self ):
      return self.__defaultValue

   def value ( self ):
      return self.__value

   def setValue ( self, value_p ):
      self.__value = value_p

   def helpString ( self ):
      result_l = "\n * " + self.__note + "\n\tkeys:"
      for nextKey in self.__keys:
         result_l += "\n\t\t" + nextKey
      if None != self.__defaultValue:
         defaultTxt_l = "\"" + str( self.__defaultValue ) + "\""
      else:
         defaultTxt_l = "[not set]"
      result_l += "\n\tdefault value:" + "\n\t\t" + defaultTxt_l
      return result_l + " \n---------------------------------"
# /class description:

## @brief Class for cmd-arguments operating 
class arguments:
   __descriptions = []
   __unknown = []
   __index = {}

   def buildIndex ( self ):
      argumentIndex_l = 0
      for argument in self.__descriptions:
         for key in argument.keys():
            self.__index[ key ] = argumentIndex_l
         argumentIndex_l += 1

   def __init__ ( self, descriptions_p ):
      self.__descriptions = descriptions_p
      self.buildIndex()
      found_l = None
      argumentType_l = "[key]"
      for argument in sys.argv[1:]:
         if "[key]" == argumentType_l:
            found_l = self.__index.get( argument )
            if None != found_l:
               if None == self.__descriptions[ found_l ].defaultValue():
                  self.__descriptions[ found_l ].setValue( "[defined]" )
               else:
                  argumentType_l = "[data]"
            else:
               self.__unknown.append( argument )
         else:
            self.__descriptions[ found_l ].setValue( argument )
            argumentType_l = "[key]"

   def value ( self, key_p ):
      return self.__descriptions[ self.__index[ key_p ] ].value()

   def helpTxt ( self ):
      result_l = " cmd-arguments:"
      for argument in self.__descriptions:
         result_l += argument.helpString()
      return result_l
      
   def unknown ( self ):
      if not len( self.__unknown ):
         return None
      result_l = "Unknown arguments:"
      for argument in self.__unknown:
         result_l += "\n\t" + argument
      return result_l
   
   def unknownList ( self ):
      return self.__unknown
# /class arguments: